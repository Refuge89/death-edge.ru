<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WoW Guild
 */
?>

</div><!-- #content -->
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info row">
            <p class="col-md-3">&copy; <a href="http://www.sedama-development.com/" target="_blank">Sedama-development.com</a>, 2014</p>
            <p class="col-md-3 col-md-offset-4">info@sedama-development.com</p>
            <p class="col-md-2">+7 (968) 604-45-71</p>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
