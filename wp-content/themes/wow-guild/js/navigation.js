/**
 * navigation.js
 *
 * Handles toggling the navigation menu for small screens.
 */
( function() {
	var container, button, menu;

	container = document.getElementById( 'site-navigation' );
	if ( ! container )
		return;

	button = container.getElementsByTagName( 'button' )[0];
	if ( 'undefined' === typeof button )
		return;

	menu = container.getElementsByTagName( 'ul' )[0];

	// Hide menu toggle button if menu is empty and return early.
	if ( 'undefined' === typeof menu ) {
		button.style.display = 'none';
		return;
	}

	if ( -1 === menu.className.indexOf( 'nav-menu' ) )
		menu.className += ' nav-menu';

	button.onclick = function() {
		if ( -1 !== container.className.indexOf( 'toggled' ) )
			container.className = container.className.replace( ' toggled', '' );
		else
			container.className += ' toggled';
	};
} )();

jQuery(document).ready( function() {

    jQuery('#bbpress-scroll-top-footer').click(function () {
        jQuery('body,html').animate({
            scrollTop: 0
        }, 500);
        return false;
    });

    if (true_posts != undefined) {
        jQuery(window).scroll(function() {
            var data = {
                'action': 'loadmore',
                'query': true_posts,
                'page' : current_page
            };

            if( (jQuery(window).scrollTop() + jQuery(window).height()) >= jQuery(document).height() - 200 && !jQuery('body').hasClass('loading')) {
                jQuery.ajax({
                    url:ajaxurl,
                    data:data,
                    type:'POST',
                    beforeSend: function( xhr) {
                        jQuery('body').addClass('loading');
                    },
                    success:function(data) {
                        if( data ) { 
                            jQuery('#true_loadmore').before(data);
                            jQuery('body').removeClass('loading');
                            current_page++;
                        }
                    }
                });
            }
        });

       jQuery(window).scroll(function () {
            if (jQuery(this).scrollTop() > 0) {
                jQuery('#scroller').fadeIn();
            } else {
                jQuery('#scroller').fadeOut();
            }
        });

        jQuery('#scroller').click(function () {
            jQuery('body,html').animate({
                scrollTop: 0
            }, 500);
            return false;
        });
    }
});
