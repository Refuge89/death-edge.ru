<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package WoW Guild
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content row">
		<?php if( is_bbpress() ) { ?>
		<div class="col-md-12">
		<?php } ?>
		<?php the_content(); ?>
		<?php if( is_bbpress() ) { ?>
		</div>
		<?php } ?>
		<?php
			wp_link_pages( array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'wow-guild' ),
				'after'  => '</div>',
			) );
		?>
		<?php if( !is_bbpress() ) { ?>
		<div id="right-sidebar" class="col-md-4">
			<?php get_sidebar(); ?>
		</div>
		<?php } ?>
	</div><!-- .entry-content -->
	<?php if( is_bbpress() ) { ?>
		<div id="bbpress-status-footer"class="row"> <?=do_shortcode('[bbp-stats]')?></div>
		<div id="bbpress-online-footer"class="row">
			<div class="col-md-5">
				<?php if (function_exists('users_online')): ?>
	    			<?php users_online(); ?>
				<?php endif; ?>
			</div>
			<div class="col-md-2 col-md-offset-2">
				<a href="/view/no-replies">Темы без ответов</a>
			</div>
			<div class="col-md-3">
				<a href="/view/popular">Самый популярный контент</a>
			</div>
		</div>
		<div id="bbpress-online-details-footer"class="row">
			<div class="col-md-6">
				<?php if (function_exists('get_users_browsing_site')): ?>
	   				<div id="useronline-browsing-site"><?php echo get_users_browsing_site(); ?></div>
				<?php endif; ?>
			</div>
			<div class="col-md-5 col-md-offset-1">
				<?php if (function_exists('get_users_browsing_page')): ?>
	   				<div id="useronline-browsing-page"><?php echo get_users_browsing_page(); ?></div>
				<?php endif; ?>
			</div>
		</div>
		<div id="bbpress-scroll-top-footer"class="row">
		</div>
	<?php } ?>
</article><!-- #post-## -->
