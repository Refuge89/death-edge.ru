<?php
/**
 * WoW Guild functions and definitions
 *
 * @package WoW Guild
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 */
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

if ( ! function_exists( 'wow_guild_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wow_guild_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on WoW Guild, use a find and replace
	 * to change 'wow-guild' to the name of your theme in all the template files
	 */
	load_theme_textdomain( 'wow-guild', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link http://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	//add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'wow-guild' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'search-form', 'comment-form', 'comment-list', 'gallery', 'caption',
	) );

	/*
	 * Enable support for Post Formats.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'image', 'video', 'quote', 'link',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wow_guild_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );
}
endif; // wow_guild_setup
add_action( 'after_setup_theme', 'wow_guild_setup' );

/**
 * Register widget area.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_sidebar
 */
function wow_guild_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Sidebar', 'wow-guild' ),
		'id'            => 'sidebar-1',
		'description'   => '',
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
}
add_action( 'widgets_init', 'wow_guild_widgets_init' );

function FAQ_news_dashboard_widget_function() {
	?>
	<p>	
		Для управления новостной лентой, необходимо перейти в раздел <a href="/wp-admin/edit.php?post_type=news">"Новости"</a> бокового меню, 
		либо выбрав соответствующий пункт в "Меню администратора". В открывшемся окне, можно изменять и удалять уже существующие новости, 
		и добавлять новые, при помощи кнопки <a href="/wp-admin/post-new.php?post_type=news">"Add News Item"</a>. Для того, чтобы добавить новую новость, 
		необходимо ввести заголовок новости, её текст и "цитату" - краткое содержание новости, или ее начало. Цитаты отображаются в ленте новостей, 
		цитата и ссылка на самую свежую новость отображается на главной страницы. После заполнения содержания новости, неоходимо нажать кнопку "Опубликовать", 
		и новость будет добавлена в ленту.
	</p>
<?php
}

function FAQ_carousel_dashboard_widget_function() {
	?>
	<p>	
		Для управления слайдером на главной странице, необходимо перейти в раздел <a href="/wp-admin/edit.php?post_type=cptbc">"Слайдер"</a> бокового меню, 
		либо выбрав соответствующий пункт в "Меню администратора". В открывшемся окне, можно изменять и удалять уже существующие изображения, 
		и добавлять новые, при помощи кнопки <a href="/wp-admin/post-new.php?post_type=cptbc">"Add new"</a>. Для того, чтобы добавить новое изображение в слайдер, 
		необходимо ввести заголовок изображения (заголовок будет расположен на главной под изображением - рыжий текст на черном фоне, 
		и задать миниатюру записи нажав на соответствующий пункт в выпадающем меню справа. <strong>ВАЖНО: Обратите внимание на то, что, 
		для соблюдения выбранных пропорций дизайна сайта, и избежания возможных ошибок, размер загружаемой вами миниатюры записи должен быть 1180x248 пикселей, и 
		размер всех миниатюр должен быть одинаков.</strong> Если вы хотите, чтобы по нажатию изображение в слайдере, происходил переход на ту или иную страницу -
		укажите адрес страницы в выпадающем меню справа в поле "Image URL", (например, http://www.youtube.com/), и поставьте галочку "Open link in new window?", 
		если хотите чтобы страница открылась в новом окне. После выполнения всех необходимых пунктов, неоходимо нажать кнопку "Опубликовать", и изображение 
		будет добавлено в слайдер на главную.
	</p>
<?php
}

function FAQ_recruit_dashboard_widget_function() {
	?>
	<p>	
		Для управления виджетом "Приоритеты рекрутинга", необходимо выбрать соответствующий пункт в "Меню администратора" - <a href="/wp-admin/widgets.php">Управление рекрутингом и другими виджетами</a>. 
		В открывшемся окне, неободимо нажать на пункт выпадающего меню "WOW Recruitment Widget: ПРИОРИТЕТЫ РЕКРУТИНГА". 
		В развернувшихся настройках, вы можете изменить приоритеты рекрутинга того или иного класса на "Высокий", "Средний" или "Низкий". 
		В поле "Note:" вы можете указывать пояснения и требования к билду будующего члена гильдии.
		После внесения всех необходимых изменений, неоходимо нажать кнопку "Сохранить".
	</p>
<?php
}

function FAQ_forum_dashboard_widget_function() {
	?>
	<p>	
		Для управления форумом, необходимо перейти в интересующий вас раздел бокового меню: <a href="/wp-admin/edit.php?post_type=forum">Форум</a>, 
		<a href="/wp-admin/edit.php?post_type=topic">Темы</a>, <a href="/wp-admin/edit.php?post_type=reply">Ответы</a>,
		либо выбрав соответствующий пункт в "Меню администратора".<br> 
		В разделе <a href="/wp-admin/edit.php?post_type=forum">Форум</a>, можно изменять и удалять уже существующие форумы, 
		а также добавлять новые ветки и разделы форума. Для добавления новой ветки форума, необходимо нажать на кнопку "Новый форум", 
		в открывшемся окне ввести название форума. Далее, в параметрах форума справа, <strong>неободимо выбрать родителя форума. 
		ВАЖНО: родителем новых форумов может являться ветка "Главная", либо одна из дочерних веток ветки "Главная". У нового форума обязательно должен быть родитель.</strong>
		Выбрав родителя форума, измените его тип: если вы создаете новый раздел форума, и родитель вашего форума - ветка "Главная", выберите тип - категория. 
		Если вы создаете форум внутри раздела форума - например "Общего раздела" - выберите тип - форум.
		Также вы можете указать статус форума, например закрыть его от добавления новых тем, а также видимость форума для посетителей. 
		После внесения всех необходимых изменений нажмите кнопку опубликовать.<br>
		В разделе <a href="/wp-admin/edit.php?post_type=topic">Темы</a>, можно изменять и удалять уже существующие темы форумов, 
		а также добавлять новые темы, прикреплять их (помещать на передний план). 
		Добавление новой темы через консоль администратора происходит аналогично добавлению ветки форума.<br>
		<a href="/wp-admin/edit.php?post_type=reply">Ответы</a>, можно изменять и удалять уже существующие ответы на форуме, 
		а также добавлять новые ответы. 
		Добавление нового ответа через консоль администратора происходит аналогично добавлению ветки форума.<br>
	</p>
<?php
}

function FAQ_users_dashboard_widget_function() {
	?>
	<p>	
		Для управления пользователями сайта, необходимо выбрать соответствующий пункт в "Меню администратора" - <a href="/wp-admin/users.php">Управление пользователями</a>. 
		В открывшемся окне, можно добавлять новых, удалять уже зарегистрированных пользователей и изменять роли пользователей на сайте и на форуме.
		Для удаления пользователей необходимо навести курсор мыши на пользователя и нажать на появившуюся ссылку "Удалить". Для изменения ролей пользователя на сайте, 
		необходимо отметить пользователя, поставив галочку напротив него, из выпадающих списков в шапке страницы выбрать роль в меню "Изменить роль на...", и "Изменить роль форума", 
		и нажать кнопку "Изменить" рядом с меню.
	</p>
<?php
}

function FAQ_errors_dashboard_widget_function() {
	?>
	<p>	
		При возникновении неполадок на сайте, вы можете отправить письмо на почту администратора, 
		сопровождающего ваш сайт - <a href="mailto:electrokoren@mail.ru">electrokoren@mail.ru</a>. 
		В письме вкрадце изложите проблему, и администратор решит ее в кратчайшие сроки.
	</p>
<?php
}

function menu_dashboard_widget_function() {
	// Dashboard menu widget
	?>
	<ul>
		<li><a href="/wp-admin/edit.php?post_type=cptbc">Управление слайдером главной страницы</a></li>
		<li><a href="/wp-admin/edit.php?post_type=news">Управление новостной лентой</a></li>
		<li><a href="/wp-admin/widgets.php">Управление рекрутингом</a></li>
		<li><a href="/wp-admin/users.php">Управление пользователями</a></li>
		<li><a href="/wp-admin/edit.php?post_type=forum">Форум, разделы форума</a></li>
		<li><a href="/wp-admin/edit.php?post_type=topic">Темы форума</a></li>
		<li><a href="/wp-admin/edit.php?post_type=reply">Ответы на форуме</a></li>
	</ul>
<?php
}

function add_dashboard_widgets() {
	wp_add_dashboard_widget('menu_dashboard_widget', 'Меню администратора', 'menu_dashboard_widget_function');
	wp_add_dashboard_widget('FAQ_news_dashboard_widget', 'Как управлять новостями?', 'FAQ_news_dashboard_widget_function');
	wp_add_dashboard_widget('FAQ_carousel_dashboard_widget', 'Как добавить/удалить изображение в слайдер на главной?', 'FAQ_carousel_dashboard_widget_function');
	wp_add_dashboard_widget('FAQ_recruit_dashboard_widget', 'Как изменить приоритеты рекрутинга?', 'FAQ_recruit_dashboard_widget_function');
	wp_add_dashboard_widget('FAQ_forum_dashboard_widget', 'Как управлять форумом?', 'FAQ_forum_dashboard_widget_function');
	wp_add_dashboard_widget('FAQ_users_dashboard_widget', 'Как управлять пользователями?', 'FAQ_users_dashboard_widget_function');
	wp_add_dashboard_widget('FAQ_errors_dashboard_widget', 'У меня что-то сломалось... Что делать?', 'FAQ_errors_dashboard_widget_function');
}
add_action('wp_dashboard_setup', 'add_dashboard_widgets' );

/**
 * Enqueue scripts and styles.
 */
function wow_guild_scripts() {
	wp_enqueue_style( 'wow-guild-style', get_stylesheet_uri() );

	wp_enqueue_script( 'wow-guild-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );

	wp_enqueue_script( 'wow-guild-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20130115', true );

	wp_enqueue_script('wow-guild-bootstrap', get_template_directory_uri(). '/bootstrap/js/bootstrap.min.js', array('jquery'));

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'wow_guild_scripts');


/**
 * Implement the Custom Header feature.
 */
//require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

function the_title_trim($title)
{
  $pattern[0] = '/Protected:/';
  $pattern[1] = '/Личное:/';
  $replacement[0] = ''; // Enter some text to put in place of Protected:
  $replacement[1] = ''; // Enter some text to put in place of Private:

  return preg_replace($pattern, $replacement, $title);
}
add_filter('the_title', 'the_title_trim');

function true_load_posts() {

	$args = unserialize(stripslashes($_POST['query']));
	$args['paged'] = $_POST['page'] + 1;
	$q = new WP_Query($args);
	if( $q->have_posts() ):
		while($q->have_posts()): $q->the_post();
			?>
			<div id="post-<?php the_ID(); ?>" type="archivenews" <?php post_class(); ?>>
	          <h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
	          <div class="post-info">
	              <span class="date published time" title="<?php the_time('c') ?>"><?php the_time('F j, Y') ?></span>
	          </div>
	          <div class="entry-content">
	                  <?php
	                  if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) {
	                    the_post_thumbnail();
	                  }
	                  ?>
	                  <div class="summary"><?php the_excerpt(); ?></div>
	          </div>
	      </div><!-- post -->
<?php
		endwhile;
	endif;
	if( $q->max_num_pages == $args['paged'] ) {
		?>
		<style>#true_loadmore{display:none;}</style>
		<?php
	}
	wp_reset_postdata();
	die();
}

function my_custom_login_logo(){
	echo '<style type="text/css">
	h1 a { background-image:url('.get_bloginfo('template_directory').'/images/loginlogo.png) !important; }
	</style>';
}
add_action('login_head', 'my_custom_login_logo');

add_action('wp_ajax_loadmore', 'true_load_posts');
add_action('wp_ajax_nopriv_loadmore', 'true_load_posts');

if (!current_user_can('administrator')):
  show_admin_bar(false);
endif;

function remove_admin_bar_links() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu('new-content');
    $wp_admin_bar->remove_menu('new-link');
	$wp_admin_bar->remove_menu('comments');
	$wp_admin_bar->remove_menu('edit');
	$wp_admin_bar->remove_menu('wp-logo');
	$wp_admin_bar->remove_menu('updates');
	$wp_admin_bar->remove_menu('appearance');
}
add_action( 'wp_before_admin_bar_render', 'remove_admin_bar_links' );

function urlExists($url = NULL) {  
    if($url == NULL) return false;  
    $ch = curl_init($url);  
    curl_setopt($ch, CURLOPT_TIMEOUT, 5);  
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);  
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);  
    $data = curl_exec($ch);  
    $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);  
    curl_close($ch);  
    if($httpcode>=200 && $httpcode<300){  
        return true;  
    } else {  
        return false;  
    }  
}

add_theme_support( 'post-thumbnails' );
add_image_size( 'post-carousel', 1180, 250, true );

if ( has_post_thumbnail() ){
    the_post_thumbnail( 'post-carousel' );
}