<?php

/**
 * Statistics Content Part
 *
 * @package bbPress
 * @subpackage Theme
 */

// Get the statistics
$stats = bbp_get_statistics(); ?>

<ul role="main" class="row">

	<?php do_action( 'bbp_before_statistics' ); ?>

	<li class="col-md-3">
		<strong><?php echo esc_html( $stats['user_count'] ); ?></strong>
		<?php _e( 'Всего пользователей', 'bbpress' ); ?>
	</li>
	<li class="col-md-3">
		<strong><?php echo esc_html( $stats['topic_count'] ); ?></strong>
		<?php _e( 'Всего тем', 'bbpress' ); ?>
	</li>
	<li class="col-md-3">
		<strong><?php echo esc_html( $stats['reply_count'] ); ?></strong>
		<?php _e( 'Всего сообщений', 'bbpress' ); ?>
	</li>
	<li class="col-md-3">
		<strong><?php echo esc_html( $stats['topic_tag_count'] ); ?></strong>
		<?php _e( 'Topic Tags', 'bbpress' ); ?>
	</li>

	<?php do_action( 'bbp_after_statistics' ); ?>

</ul>

<?php unset( $stats );