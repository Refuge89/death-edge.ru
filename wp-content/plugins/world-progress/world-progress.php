<?php
/*
Plugin Name: World progress
Plugin URI: http://Sedama-development.com
Author: Sedama-development
Author URI: http://Sedama-development.com
*/

function world_progress_widget($args) {

	extract($args);
	
	echo $before_widget; 
	echo $before_title;
	echo get_option('world_progress_widget_title'); 
	echo $after_title; ?>
	<div id="world-progress-widget">
		<?php if( urlExists('http://www.wowprogress.com/guild/eu/Свежеватель-душ/Смертельная+Грань/rating.tier17_25/json_rank') ) { ?>
			<div class="world-progress-item">
				<img src="/wp-content/plugins/world-progress/images/t1725.jpg" alt="tier-image">
				<?php 
					$rank_array = json_decode(file_get_contents('http://www.wowprogress.com/guild/eu/Свежеватель-душ/Смертельная+Грань/rating.tier17_20/json_rank'), true);
				?>
				<h3>Rank <?=$rank_array['realm_rank']?></h3>
				<p>Tier 17(20)</p>
			</div>
			<div class="world-progress-item">
				<img src="/wp-content/plugins/world-progress/images/t1725r.jpg" alt="tier-image">
				<?php 
					$rank_array = json_decode(file_get_contents('http://www.wowprogress.com/guild/eu/Свежеватель-душ/Смертельная+Грань/rating.tier17_25/json_rank'), true);
				?>
				<h3>Rank <?=$rank_array['realm_rank']?></h3>
				<p>Tier 17(25)</p>
			</div>
			<div class="world-progress-item">
				<img src="/wp-content/plugins/world-progress/images/t1710r.jpg" alt="tier-image">
				<?php 
					$rank_array = json_decode(file_get_contents('http://www.wowprogress.com/guild/eu/Свежеватель-душ/Смертельная+Грань/rating.tier17_10/json_rank'), true);
				?>
				<h3>Rank <?=$rank_array['realm_rank']?></h3>
				<p>Tier 17(10)</p>
			</div>
			<div class="world-progress-item">
				<img src="/wp-content/plugins/world-progress/images/t1710.jpg" alt="tier-image">
				<?php 
					$rank_array = json_decode(file_get_contents('http://www.wowprogress.com/guild/eu/Свежеватель-душ/Смертельная+Грань/rating.tier16_20/json_rank'), true);
				?>
				<h3>Rank <?=$rank_array['realm_rank']?></h3>
				<p>Tier 16(20)</p>
			</div>
			<div class="world-progress-item">
				<img src="/wp-content/plugins/world-progress/images/t12.png" alt="tier-image">
				<?php 
					$rank_array = json_decode(file_get_contents('http://www.wowprogress.com/guild/eu/Свежеватель-душ/Смертельная+Грань/rating.tier16_25/json_rank'), true);
				?>
				<h3>Rank <?=$rank_array['realm_rank']?></h3>
				<p>Tier 16(25)</p>
			</div>
			<div class="world-progress-item">
				<img src="/wp-content/plugins/world-progress/images/t11.png" alt="tier-image">
				<?php 
					$rank_array = json_decode(file_get_contents('http://www.wowprogress.com/guild/eu/Свежеватель-душ/Смертельная+Грань/rating.tier16_10/json_rank'), true);
				?>
				<h3>Rank <?=$rank_array['realm_rank']?></h3>
				<p>Tier 16(10)</p>
			</div><br>
			<div class="world-progress-item">
				<img src="/wp-content/plugins/world-progress/images/lich.png" alt="tier-image">
				<?php 
					$rank_array = json_decode(file_get_contents('http://www.wowprogress.com/guild/eu/Свежеватель-душ/Смертельная+Грань/rating.tier15_25/json_rank'), true);
				?>
				<h3>Rank <?=$rank_array['realm_rank']?></h3>
				<p>Tier 15(25)</p>
			</div>
			<div class="world-progress-item">
				<img src="/wp-content/plugins/world-progress/images/boss.png" alt="tier-image">
				<?php 
					$rank_array = json_decode(file_get_contents('http://www.wowprogress.com/guild/eu/Свежеватель-душ/Смертельная+Грань/rating.tier15_10/json_rank'), true);
				?>
				<h3>Rank <?=$rank_array['realm_rank']?></h3>
				<p>Tier 15(10)</p>
			</div>
		<?php } else { ?>
			<p id="world-progress-error">Сервер статистики недоступен</p>
		<?php } ?>
	</div>
<?php	echo $after_widget; 

}

function register_world_progress_widget() {
	register_sidebar_widget('World progress', 'world_progress_widget');
	register_widget_control('World progress', 'world_progress_widget_control' );
}


function world_progress_widget_control() {
	
	if (!empty($_REQUEST['world_progress_widget_title'])) {
		update_option('world_progress_widget_title', $_REQUEST['world_progress_widget_title']);
	}
?>
	Заголовок&nbsp;:&nbsp;<input type="text" name="world_progress_widget_title" value="<?=get_option('world_progress_widget_title')?>">
<?

}
add_action('init', 'register_world_progress_widget');