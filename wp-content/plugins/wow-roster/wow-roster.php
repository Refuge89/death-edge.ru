<?php
/*
Plugin Name: WoW Roster
Plugin URI: http://Sedama-development.com
Author: Sedama-development
Author URI: http://Sedama-development.com
*/

function wow_roster_widget($args) {

	extract($args);

	$classes = array('Воин', 'Паладин', 'Охотник', 'Разбойник', 'Жрец', 'Рыцарь смерти', 'Шаман', 'Маг', 'Чернокнижник', 'Монах', 'Друид');
	$members_per_class = 4;
	$guild = 'Смертельная%20Грань';
	$realm = 'Свежеватель%20душ';

	if( urlExists("http://eu.battle.net/api/wow/guild/$realm/$guild?fields=members") ) {

		$temp = json_decode(file_get_contents("http://eu.battle.net/api/wow/guild/$realm/$guild?fields=members"), true);
		$temp = $temp['members'];

		$temp_results = array();

		foreach ($temp as $key => $value) {
			$temp_results[] = array('name' => $value['character']['name'], 'class' => $value['character']['class'], 'rank' => $value['rank']);
		}

		usort($temp_results, 'orderByRank');

		$results = array();

		foreach ($temp_results as $key => $value) {
			$results[$value['class']][] = $value['name'];
		}

		unset($temp);
		unset($temp_results);
		
		echo $before_widget; 
		echo $before_title;
		echo get_option('wow_roster_widget_title'); 
		echo $after_title; ?>

		<div id="wow-roster-widget">
			<div class="row">
	        <?php for($i = 1; $i <= count($results); $i++): ?>
	            <div id="wow-roster-class-<?=$i?>" class="wow-roster-item col-md-4">
	            	<div style="background: #3d3d3d url(/wp-content/plugins/wow-roster/images/class<?=$i?>.png) center right no-repeat;">
		                <h2><?=$classes[$i-1]?></h2>
		                    <?php
		                        for($j = 0; $j < $members_per_class; $j++) {?>
		                        	<p>
		                        		<a href="http://eu.battle.net/wow/ru/character/<?=$realm?>/<?=$results[$i][$j]?>/simple" target="_blank"><?=$results[$i][$j]?></a>
	                        		</p>
		                        <?php }
		                    ?>
	                </div>
	            </div>
	            <?php if($i % 3 == 0) { 
	                echo '</div><div class="row">'; 
	            } ?>
	        <?php endfor;?>
	        </div>
		</div>
	<?php } else { ?>
		<p id="world-roster-error">
			Сервер статистики недоступен.<br>
			Попробуйте обновить страницу позже.
		</p>
	<?php } ?>
<?php	echo $after_widget; 

}

function register_wow_roster_widget() {
	register_sidebar_widget('WoW Roster', 'wow_roster_widget');
	register_widget_control('WoW Roster', 'wow_roster_widget_control' );
}


function wow_roster_widget_control() {
	
	if (!empty($_REQUEST['wow_roster_widget_title'])) {
		update_option('wow_roster_widget_title', $_REQUEST['wow_roster_widget_title']);
	}
?>
	Заголовок&nbsp;:&nbsp;<input type="text" name="wow_roster_widget_title" value="<?=get_option('wow_roster_widget_title')?>">
<?

}

add_action('init', 'register_wow_roster_widget');

function orderByRank($a, $b) {
    return strnatcmp($a['rank'], $b['rank']);
}